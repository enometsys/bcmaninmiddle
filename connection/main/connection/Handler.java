package main.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

import main.Main;
import main.packet.Packet;

public abstract class Handler extends Thread{
	private final String TAG;
	
	protected Main main;
	private boolean connected;
	
	private Socket socket;
	private ObjectOutputStream out;											//outputStream
	private ObjectInputStream in;											//inputSteam
	
	protected InetAddress inetAddress;
	
	public Handler(String TAG, Main main, Socket socket){
		this.TAG = TAG;
		this.main = main;
		this.socket = socket;
		
		try{
			this.inetAddress = socket.getInetAddress();
		}catch (Exception e){
			e.printStackTrace();
			Main.log.e(TAG, e.getMessage());
		}
		
		try {
			out = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
			Main.log.e(TAG, "failed init out");
		}
		
		try {
			in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
			Main.log.e(TAG, "failed init in");
		}
		
		Main.log.d(TAG, "constructed");
	}
	
	@Override
	public void run() {
		connected = true;

		while(isConnected()){
			try {
				receivePacket((Packet)in.readObject());
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				Main.log.e(TAG, "startReceiving()", e.getMessage());
				
			} catch (IOException e) {
				e.printStackTrace();
				Main.log.e(TAG, "startReceiving()", e.getMessage());
				
				try {
					in = new ObjectInputStream(socket.getInputStream());
					continue;
				} catch (IOException e1) {
					e1.printStackTrace();
					Main.log.e(TAG, "startReceiving()#trying to rebuild in", e.getMessage());
				}

				disconnect();
			}
		}
		
		main.serverDisconnected();
	}
	
	private void receivePacket(Packet pckt){
		if (TAG.equalsIgnoreCase(ServerHandler.class.getSimpleName())){
			//From server
			
			main.sendToClient(processPacket(pckt));
			
		}else if (TAG.equalsIgnoreCase(ClientHandler.class.getSimpleName())){
			//From client
			
			main.sendToServer(processPacket(pckt));
		}
	}
	
	protected abstract Packet processPacket(Packet pckt);
	
	public void sendPacket(Packet pckt){
		if (isConnected()){
			try {
				out.writeObject(pckt);
				packetSent(pckt);
				Main.log.d(TAG, "packet " + pckt.getType() + " sent...");
				
			} catch (IOException e) {
				e.printStackTrace();
				Main.log.e(TAG, "sendPacket()", e.getMessage());
				try {
					out = new ObjectOutputStream(socket.getOutputStream());
				} catch (IOException e1) {
					e1.printStackTrace();
					Main.log.e(TAG, "sendPacket()# reinit out", e.getMessage());
					disconnect();
				}
			}
		}else Main.log.e(TAG, "disconnected");
	}
	
	protected abstract void packetSent(Packet pckt);
	
	private synchronized boolean isConnected() {
		return connected;
	}
	
	public void disconnect(){
		synchronized(this){
			connected = false;
		}
		Main.log.d(TAG, "disconnecting...");
		try {
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
			Main.log.e(TAG, "disconnect()", e.getMessage());
		}
	}
}
