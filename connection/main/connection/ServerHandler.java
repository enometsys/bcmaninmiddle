package main.connection;

import java.net.Socket;

import main.Main;
import main.packet.Packet;

public class ServerHandler extends Handler{
	private static final String TAG = ServerHandler.class.getSimpleName();
	
	public ServerHandler(Main main, Socket socket) {
		super(TAG, main, socket);
	}

	@Override
	protected Packet processPacket(Packet pckt) {
		main.packetReceived(inetAddress.getHostAddress(), pckt.getType().toString());
		
		return pckt;
	}

	@Override
	protected void packetSent(Packet pckt) {
		main.packetSent(inetAddress.getHostAddress(), pckt.getType().toString());
		
	}	
}
