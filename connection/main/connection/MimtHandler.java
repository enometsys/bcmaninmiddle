package main.connection;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import main.Main;

public class MimtHandler extends Thread{
	private static final String TAG = MimtHandler.class.getSimpleName();
	
	private Main main;
	private boolean serverConnected;
	
	private static int MIMT_PORT = 5000;									//port for listening
	private ServerSocket serverSocket;										//server socket
	
	public MimtHandler(Main main){
		this.main = main;
	}
	
	@Override
	public void run() {
		Socket tempSocket;
		ClientHandler tempClient;
		
		while(serverConnected){
			try {
				serverSocket = new ServerSocket(MIMT_PORT);
				Main.log.d(TAG, "MimtServerControl initialized.");
				
				while(true){
					Main.log.d(TAG, "listening at " + serverSocket.getInetAddress()
									.getHostAddress() + ":" + serverSocket.getLocalPort());
					
					tempSocket = serverSocket.accept();
					
					tempClient = new ClientHandler(main, tempSocket);
					tempClient.start();
					
					main.addClient(tempSocket.getInetAddress().getHostAddress(), tempClient);
				}
				
			} catch (IOException e) {
				e.printStackTrace();
				Main.log.e(TAG, e.getMessage());
			}
		}
	}

	public void serverDisconnected(){
		serverConnected = false;
		Main.log.d(TAG, "stopping accept...");
		try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
			Main.log.e(TAG, "stopAccepting()", e.getMessage());
		}
	}
}
