package main;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import main.connection.ClientHandler;
import main.connection.MimtHandler;
import main.connection.ServerHandler;
import main.logger.Logger;
import main.packet.Packet;

public class Main {
	private static final String TAG = Main.class.getSimpleName();
	public static final Logger log = new Logger();
	
	private int SERVER_PORT = 3000;
	private Ui ui;
	
	private Map<String, ClientHandler> clients;
	private ServerHandler server;
	private MimtHandler mimt;
	
	public Main(){
		ui = new Ui();

		connectToServer();
		mimt = new MimtHandler(this);
		mimt.start();
		clients = new HashMap<String, ClientHandler>();
	}
	
	public void connectToServer(){
		Socket serverSocket;
		
		//TODO: get ipAddress from input box
		String ipAddress = "0.0.0.0";
		
		try {
			serverSocket = new Socket(ipAddress, SERVER_PORT);			
			server = new ServerHandler(this, serverSocket);
			server.start();
			
		} catch (IOException e) {
			e.printStackTrace();
			Main.log.e(TAG, e.getMessage());
		}
	}
	
	public void packetReceived(String from, String packet){
		ui.packetReceived(from, packet);
	}
	
	public void packetSent(String to, String packet){
		ui.packetSent(to, packet);
	}
	
	public void addClient(String inetAddress, ClientHandler cl){
		log.i(TAG, "Client " + inetAddress + "added!");
		clients.put(inetAddress, cl);
	}
	
	public void serverDisconnected(){
		log.d(TAG, "Server disconnected!");
		server = null;
	}
	
	public void clientDisconnected(String inetAddress){
		if (clients.containsKey(inetAddress)){
			log.i(TAG, "Client " + inetAddress + "disconnected!");
			clients.remove(clients.get(inetAddress));
		}
	}
	
	public void sendToServer(Packet pckt){
		if (server == null){
			log.d(TAG, "Server disconnected!");
			return;
		}
		
		server.sendPacket(pckt);
	}
	
	public void sendToClient(Packet pckt){
		if (!clients.containsKey(pckt.getReceiverAddress())){
			log.d(TAG, "Client DNE!");
			return;
		}
		
		clients.get(pckt.getReceiverAddress()).sendPacket(pckt);
	}
}
