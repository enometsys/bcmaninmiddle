package main;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import net.miginfocom.swing.MigLayout;

public class Ui {

	private JFrame frame;
	private JTable tblReceiving;
	private JTable tblSending;
	/**
	 * Create the application.
	 */
	public Ui() {
		initialize();
		frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Battle Clans Server");
		frame.setBounds(100, 100, 605, 484);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new MigLayout("", "[grow][grow]", "[][][grow][grow][][grow]"));
		
		JLabel lblNewLabel = new JLabel("Receiving");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblNewLabel, "cell 0 0 1 2,alignx center,aligny center");
		
		JLabel lblChatMessages = new JLabel("Sending");
		lblChatMessages.setHorizontalAlignment(SwingConstants.CENTER);
		frame.getContentPane().add(lblChatMessages, "cell 1 1,alignx center,aligny center");
		
		tblReceiving = new JTable();
		tblReceiving.setBorder(new LineBorder(new Color(0, 0, 0)));
		
		frame.getContentPane().add(tblReceiving, "cell 0 2 1 4,grow");
		
		tblSending = new JTable();
		tblSending.setBorder(new LineBorder(new Color(0, 0, 0)));
		frame.getContentPane().add(tblSending, "cell 1 2 1 4,grow");
	}
	
	public void packetReceived(String from, String packet){
		
	}
	
	public void packetSent(String to, String packet){
		
	}
	
}
